**Meta-Analysis Review Code**

This code conducts a meta-analysis of PubMed articles to track the research trends in stem cells, systems biology, and artificial intelligence from 2000 to 2024. The analysis involves generating keyword-based search counts, plotting the data, and creating visualizations such as bar plots and heatmaps.

**Libraries and Dependencies**
    - The following R libraries are utilized:

- tidyverse: Data manipulation and visualization
- glue: String interpolation
- ggbreak: Breaks in 'ggplot2' axes
- ComplexHeatmap: Advanced heatmaps
- ggforce: Additional 'ggplot2' facets
- xml2: XML parsing
- ggplot2: Data visualization
- dplyr: Data manipulation
- maps: Geographic maps
- circlize: Circular visualization
- ggrepel: Repulsive text and label geoms
- gridExtra: Miscellaneous functions for 'grid' graphics
- rentrez: Interface with the NCBI EUtils API

        
**Data Collection**
Keyword Definitions
Keywords are defined for three main research areas:

- stem_cell: Stem cell research
- syst_Biol: Systems biology
- a_intelig: Artificial intelligence


**Search Counts**
Search counts are collected for each keyword from PubMed for the years 2000 to 2024. This involves querying PubMed using the rentrez library.

**Data Transformation**
The counts are transformed and prepared for visualization. The data is reshaped using pivot_longer and filtered to exclude the year 2024 for plotting purposes.

**Visualizations**
Bar Plots
Two types of bar plots are generated:
- Yearly Counts Plot: Shows the yearly publication counts for each keyword.
- Cumulative Counts Plot: Displays the total publication counts over the years for each keyword.

**Line Plots for Combined Keywords**
Specific searches combining keywords (e.g., "stem cell AND systems biology") are performed. Plots are created to show the yearly trends for these combined searches.

**Heatmap**
A heatmap is generated to visualize the distribution of publication counts for various stem cell types across different methods (systems biology, machine learning, deep learning).

**World Map**
The code also includes a geographical analysis, plotting the global distribution of publications based on country for the combined keyword searches.

**Bar Plot by Country**
A bar plot is created to show the frequency of publications by country.

**Outputs**
All visualizations are saved as TIFF files with specified dimensions and resolution.

Example File Outputs:
- barplot_all.tiff: Yearly counts for stem cell, systems biology, and AI.
- barplot_all_count.tiff: Cumulative counts for each keyword.
- heatmap.tiff: Heatmap of publication counts by stem cell types and methods.
- map_mundi_AI+SC.tiff: World map for AI and stem cell research.
- barplotAI+SC.tiff: Bar plot for AI and stem cell research by country.
- Additional Analysis
- Specific cell types (Neural, Hematopoietic, Embryonic, Adult, Induced Pluripotent, Mesenchymal) are analyzed with systems biology, machine learning, and deep learning.
- The geographical distribution of publications is also analyzed separately for different combined keyword searches.
- This script provides a comprehensive overview of publication trends in stem cell research combined with systems biology and AI, facilitating a meta-analysis of the scientific literature in these fields.
